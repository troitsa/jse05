package ru.vlasova.iteco.taskmanager.service;

import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.repository.ProjectRepository;
import ru.vlasova.iteco.taskmanager.util.DateUtil;

import java.util.List;

public class ProjectService extends AbstractService {

    private ProjectRepository projectRepository;

    private TaskService taskService;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    public Project findOne(String id) {
        return projectRepository.findOne(id);
    }

    public void merge(Project project) {
        projectRepository.merge(project);
    }

    public Project insert(String name, String description, String dateStart, String dateFinish) {
        boolean checkGeneral = checkNull(name, description, dateStart, dateFinish);
        if (!checkGeneral) return null;
        Project project = new Project(name, description, DateUtil.parseDateFromString(dateStart), DateUtil.parseDateFromString(dateFinish));
        return project;
    }

    public void persist(Project project) {
        try {
            projectRepository.persist(project);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Project> findAll() {
        List<Project> projects = projectRepository.findAll();
        return projects;
    }

    public void remove(int index) {
        Project project = getProjectByIndex(index);
        projectRepository.remove(project.getId());
        taskService.removeTasksByProject(project);
    }

    public Project getProject(int index) {
        return getProjectByIndex(index);
    }

    public void removeAll() {
        List<Task> taskList = taskService.findAll();
        for (Task task : taskList) {
            if (!task.getProjectId().isEmpty()) {
                taskService.remove(task);
            }
        }
        projectRepository.removeAll();
    }

    public Project getProjectByIndex(int index) {
        return projectRepository.findAll().get(index);
    }

}
