package ru.vlasova.iteco.taskmanager.service;

import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;
import ru.vlasova.iteco.taskmanager.repository.TaskRepository;
import ru.vlasova.iteco.taskmanager.util.DateUtil;

import java.util.ArrayList;
import java.util.List;

public class TaskService extends AbstractService {

    private TaskRepository taskRepository;

    private ProjectService projectService;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    public Task insert(String name, String description, String dateStart, String dateFinish) {
        boolean checkGeneral = checkNull(name, description, dateStart, dateFinish);
        if (!checkGeneral) return null;
        Task task = new Task(name, description, DateUtil.parseDateFromString(dateStart), DateUtil.parseDateFromString(dateFinish));
        return task;
    }

    public void persist(Task task) {
        try {
            taskRepository.persist(task);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Task> getTasks(int projectIndex) {
        List<Project> projectList = projectService.findAll();
        String projectId = projectList.get(projectIndex).getId();
        List<Task> tasks = getTasksByProject(projectId);
        return tasks;
    }

    public List<Task> findAll() {
        List<Task> tasks = taskRepository.findAll();
        return tasks;
    }

    public void remove(Task task) {
        if (task == null) return;
        taskRepository.remove(task.getId());
    }

    public void remove(int id) {
        Task task = getTask(id);
        if (task == null) return;
        remove(task);
    }

    public Task getTask(int index) {
        return getTaskByIndex(index);
    }

    public void removeTasksByProject(Project project) {
        List<Task> taskList = getTasksByProject(project.getId());

        for (Task task : taskList) {
            taskRepository.remove(task.getId());
        }
    }

    public void removeAll() {
        taskRepository.removeAll();
    }

    public int countTask() {
        return taskRepository.findAll().size();
    }

    public int countTaskByProject(Project project) {
        return getTasksByProject(project.getId()).size();
    }

    public Task getTaskByIndex(int index) {
        Task task = taskRepository.findAll().get(index);
        return task;
    }

    public List<Task> getTasksByProject(String projectId) {
        List<Task> listTasks = new ArrayList<>();
        List<Task> tasks = taskRepository.findAll();
        for (Task task : tasks) {
            if (task.getProjectId().equals(projectId)) {
                listTasks.add(task);
            }
        }
        return listTasks;
    }

}
