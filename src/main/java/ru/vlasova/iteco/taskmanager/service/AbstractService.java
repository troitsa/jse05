package ru.vlasova.iteco.taskmanager.service;

public abstract class AbstractService {

    public boolean checkNull(String name, String description, String dateStart, String dateFinish) {
        boolean checkName = (name != null) &&  (name.trim().length() != 0);
        boolean checkDescription = (description != null) &&  (description.trim().length() != 0);
        boolean checkDateStart = (dateStart != null) &&  (dateStart.trim().length() != 0);
        boolean checkDateFinish = (dateFinish != null) &&  (dateFinish.trim().length() != 0);
        boolean checkAll = checkName && checkDescription && checkDateStart && checkDateFinish;
        return checkAll;
    }

}
