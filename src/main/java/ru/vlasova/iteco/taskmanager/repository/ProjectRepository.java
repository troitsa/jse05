package ru.vlasova.iteco.taskmanager.repository;

import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.*;

public class ProjectRepository implements IRepository<Project> {

    private Map<String, Project> projects = new HashMap();

    @Override
    public List<Project> findAll() {
        return new ArrayList<>(projects.values());
    }

    @Override
    public Project findOne(String id) {
        return projects.get(id);
    }

    @Override
    public Project persist(Project project) throws DuplicateException {
        if (projects.containsKey(project.getId())) throw new DuplicateException("Such project exists.");
        projects.put(project.getId(), project);
        return project;
    }

    @Override
    public void merge(Project project) {
        projects.put(project.getId(), project);
    }

    @Override
    public void remove(String id) {
        projects.remove(id);
    }

    @Override
    public void removeAll() {
        projects.clear();
    }

}
