package ru.vlasova.iteco.taskmanager.repository;

import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskRepository implements IRepository<Task> {

    private Map<String, Task> tasks = new HashMap<>();

    @Override
    public List<Task> findAll() {
        return new ArrayList<Task>(tasks.values());
    }

    @Override
    public Task findOne(String id) {
        return tasks.get(id);
    }

    @Override
    public Task persist(Task task) throws DuplicateException {
        if (tasks.containsKey(task.getId())) throw new DuplicateException("Such project exists.");
        tasks.put(task.getId(), task);
        return task;
    }

    @Override
    public void merge(Task task) {
        tasks.put(task.getId(), task);
    }

    @Override
    public void remove(String id) {
        tasks.remove(id);
    }

    @Override
    public void removeAll() {
        tasks.clear();
    }

}
