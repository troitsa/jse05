package ru.vlasova.iteco.taskmanager;

import ru.vlasova.iteco.taskmanager.command.*;
import ru.vlasova.iteco.taskmanager.context.Bootstrap;

public class App {

    private final static Class[] CLASSES = new Class[] { ProjectClearCommand.class, ProjectCreateCommand.class,
            ProjectEditCommand.class, ProjectListCommand.class, ProjectRemoveCommand.class,
            TaskAttachCommand.class, TaskClearCommand.class, TaskCreateCommand.class, TaskEditCommand.class,
            TaskListByProjectCommand.class, TaskListCommand.class, TaskRemoveCommand.class,
            TaskDetachCommand.class, HelpCommand.class, ExitCommand.class };

    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(CLASSES).start();
    }

}