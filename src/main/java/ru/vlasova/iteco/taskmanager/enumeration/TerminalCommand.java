package ru.vlasova.iteco.taskmanager.enumeration;

public enum TerminalCommand {
    PROJECT_CLEAR,
    PROJECT_CREATE,
    PROJECT_LIST,
    PROJECT_EDIT,
    PROJECT_REMOVE,
    TASKS_CLEAR,
    TASK_CREATE,
    TASK_LIST,
    TASK_EDIT,
    TASK_REMOVE,
    EMPTY,
    HELP,
    EXIT
}
