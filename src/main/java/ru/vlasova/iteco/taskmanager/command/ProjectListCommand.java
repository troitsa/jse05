package ru.vlasova.iteco.taskmanager.command;

public class ProjectListCommand extends AbctractProjectCommand {

    @Override
    public String getName() {
        return "project_list";
    }

    @Override
    public String getDescription() {
        return "Show all projects";
    }

    @Override
    public void execute() {
        printProjectList();
    }

}