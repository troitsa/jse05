package ru.vlasova.iteco.taskmanager.command;

import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.service.ProjectService;

import java.util.List;

public abstract class AbctractProjectCommand extends AbstractCommand {

    public void printProjectList() {
        ProjectService projectService = bootstrap.getProjectService();
        List<Project> projectList = projectService.findAll();
        if(projectList != null && projectList.size()!=0) {
            int i = 1;
            for (Project project : projectList) {
                System.out.println(i++ + ": " + project);
            }
        }
    }

}
