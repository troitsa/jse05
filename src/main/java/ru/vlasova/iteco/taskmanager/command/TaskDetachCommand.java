package ru.vlasova.iteco.taskmanager.command;

import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.service.TaskService;

import java.io.IOException;
import java.util.List;

public class TaskDetachCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task_detach";
    }

    @Override
    public String getDescription() {
        return "Detach task from project";
    }

    @Override
    public void execute() throws IOException {
        TaskService taskService = bootstrap.getTaskService();
        System.out.println("Choose the task and type the number");
        List<Task> taskList = taskService.findAll();
        if (taskList.size() !=0) {
            printTaskList(taskList);
            int index = Integer.parseInt(reader.readLine()) - 1;
            Task task = taskService.getTask(index);
            if (task != null) {
                task.setProjectId("");
                System.out.println("Task detached.");
            }
        }
    }

}