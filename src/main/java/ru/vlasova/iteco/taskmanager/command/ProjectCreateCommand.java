package ru.vlasova.iteco.taskmanager.command;

import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.service.ProjectService;

import java.io.IOException;

public class ProjectCreateCommand extends AbctractProjectCommand {

    @Override
    public String getName() {
        return "project_create";
    }

    @Override
    public String getDescription() {
        return "Create new project";
    }

    @Override
    public void execute() throws IOException {
        ProjectService projectService = bootstrap.getProjectService();
        System.out.println("Creating project. Set name: ");
        String name = reader.readLine();
        System.out.println("Input description: ");
        String description = reader.readLine();
        System.out.println("Set start date: ");
        String dateStart = reader.readLine();
        System.out.println("Set end date: ");
        String dateFinish = reader.readLine();
        Project project = projectService.insert(name, description, dateStart,dateFinish);
        if(project == null) {
            System.out.println("Project is not created. Try again");
        } else {
            projectService.persist(project);
            System.out.println("Project created.");
        }
    }

}