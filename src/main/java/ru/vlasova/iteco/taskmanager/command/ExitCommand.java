package ru.vlasova.iteco.taskmanager.command;

import java.io.IOException;

public class ExitCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Exit from application.";
    }

    @Override
    public void execute() throws IOException {
        reader.close();
        System.exit(1);
    }

}