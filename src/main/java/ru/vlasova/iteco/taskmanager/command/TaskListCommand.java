package ru.vlasova.iteco.taskmanager.command;

import ru.vlasova.iteco.taskmanager.entity.Task;

import java.util.List;

public class TaskListCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task_list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks";
    }

    @Override
    public void execute() {
        List<Task> taskList = bootstrap.getTaskService().findAll();
        if(taskList.size()==0) {
            System.out.println("There are no tasks.");
        }
        else {
            printTaskList(taskList);
        }
    }

}