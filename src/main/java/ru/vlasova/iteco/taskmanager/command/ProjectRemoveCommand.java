package ru.vlasova.iteco.taskmanager.command;

import ru.vlasova.iteco.taskmanager.service.ProjectService;

import java.io.IOException;

public class ProjectRemoveCommand extends AbctractProjectCommand {

    @Override
    public String getName() {
        return "project_remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project";
    }

    @Override
    public void execute() throws IOException {
        ProjectService projectService = bootstrap.getProjectService();
        printProjectList();
        System.out.println("To delete project choose the project and type the number");
        int index = Integer.parseInt(reader.readLine())-1;
        projectService.remove(index);
        System.out.println("Project delete.");
    }

}