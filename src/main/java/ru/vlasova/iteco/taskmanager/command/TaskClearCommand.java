package ru.vlasova.iteco.taskmanager.command;

import ru.vlasova.iteco.taskmanager.service.TaskService;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task_clear";
    }

    @Override
    public String getDescription() {
        return "Remove all tasks";
    }

    @Override
    public void execute() {
        TaskService taskService = bootstrap.getTaskService();
        taskService.removeAll();
        System.out.println("All tasks deleted.");
    }

}