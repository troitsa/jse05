package ru.vlasova.iteco.taskmanager.command;

import ru.vlasova.iteco.taskmanager.service.ProjectService;

public class ProjectClearCommand extends AbctractProjectCommand {

    @Override
    public String getName() {
        return "project_clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects";
    }

    @Override
    public void execute() {
        ProjectService projectService = bootstrap.getProjectService();
        projectService.removeAll();
        System.out.println("All projects deleted.");
    }

}