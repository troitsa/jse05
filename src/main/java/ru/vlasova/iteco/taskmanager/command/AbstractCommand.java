package ru.vlasova.iteco.taskmanager.command;

import ru.vlasova.iteco.taskmanager.context.Bootstrap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public abstract class AbstractCommand {

    public final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    protected Bootstrap bootstrap;

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws IOException;

}