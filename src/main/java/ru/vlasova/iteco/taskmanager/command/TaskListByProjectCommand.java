package ru.vlasova.iteco.taskmanager.command;

import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.service.TaskService;

import java.io.IOException;
import java.util.List;

public class TaskListByProjectCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task_list_by_project";
    }

    @Override
    public String getDescription() {
        return "Show tasks by selected project";
    }

    @Override
    public void execute() throws IOException {
        TaskService taskService = bootstrap.getTaskService();
        System.out.println("Please, choose the project and type the number");
        printProjectList();
        int index = Integer.parseInt(reader.readLine())-1;
        List<Task> taskList = taskService.getTasks(index);
        if(taskList.size()==0) {
            System.out.println("There are no tasks.");
        }
        else {
            printTaskList(taskList);
        }
    }

}