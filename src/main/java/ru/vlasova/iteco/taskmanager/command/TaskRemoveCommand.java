package ru.vlasova.iteco.taskmanager.command;

import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.service.TaskService;

import java.io.IOException;
import java.util.List;

public class TaskRemoveCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task_remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task";
    }

    @Override
    public void execute() throws IOException {
        TaskService taskService = bootstrap.getTaskService();
        System.out.println("Choose the task and type the number");
        List<Task> taskList = taskService.findAll();
        if (taskList.size() !=0) {
            printTaskList(taskList);
            int index = Integer.parseInt(reader.readLine()) - 1;
            Task task = taskService.getTask(index);
            if (task != null) {
                taskService.remove(task);
                System.out.println("Task deleted.");
            }
        }
    }

}